﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Calender
{
    public partial class Form1 : Form
    {
        string outlook = "H:\\Remindlist.txt";
        string[] Remind = new string[373];
        
        public Form1()
        {
            InitializeComponent();
            for (int i = 1; i<=12; i++)
                comboMonth.Items.Add(i);
            for (int i = 1; i <= 31; i++)
                comboDate.Items.Add(i);
            comboDate.Click += ButtonClick;
            comboMonth.Click += ButtonClick;
            TextWriter tw = new StreamWriter(outlook, true);
            tw.Close();
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            File.WriteAllText(outlook, String.Empty);
            Remind[(Convert.ToInt32(comboMonth.Text)-1)*31 + Convert.ToInt32(comboDate.Text)] = txtRemind.Text;
            MessageBox.Show("Saved successful.");
            File.WriteAllLines(outlook, Remind);
        }

        private void ButtonClick(object sender, EventArgs e)
        {
            txtRemind.ReadOnly = true;
            btnChange.Enabled = false;
            btnDelete.Enabled = false;

        }

        private void btnLookUp_Click(object sender, EventArgs e)
        {
            StreamReader stream = new StreamReader(@outlook);
            for(int num = 0; num < Remind.Length; num++)
            {
                Remind[num] = stream.ReadLine();
            }

            if ((comboMonth.Text == "") || (comboDate.Text == ""))
            {
                MessageBox.Show("Please fill in the date and month.");
                return;
            }
            txtRemind.ReadOnly = false;
            btnChange.Enabled = true;
            btnDelete.Enabled = true;
            string s = Remind[(Convert.ToInt32(comboMonth.Text) - 1) * 31 + Convert.ToInt32(comboDate.Text)];
            if (((s == null) || (s == "")) || (s == "No activity for this day."))
            {
                MessageBox.Show("No activity for this day.");
                txtRemind.Text = "No activity for this day.";
            }
            else txtRemind.Text = s;
            stream.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            File.WriteAllText(outlook, String.Empty);
            Remind[(Convert.ToInt32(comboMonth.Text)-1)*31 + Convert.ToInt32(comboDate.Text)] = "";
            txtRemind.Text = "No activity for this day.";
            MessageBox.Show("Deleted Saved Reminder.");
            File.WriteAllLines(outlook, Remind);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
