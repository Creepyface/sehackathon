﻿using System;
using System.Collections.Generic;
using System.Text;

namespace hackathonAdministration
{
    class Teacher
    {
        public string firstName;
        public string lastName;
        public int gender; //0 = female 1 = male 2 = other
        public int id;
        public DateTime birthday = new DateTime();
        
    }
    class Student
    {
        public string firstName;
        public string lastName;
        public int gender;
        public int id;
        public DateTime birthday = new DateTime();
    }
}
