﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace InitManager
{
    class Start
    {
        
        static void Main(string[] args)
        {
            StreamReader serviceReader = new StreamReader(@"ProcessList.txt");
            List<string> serviceList = new List<string>();

            while(serviceReader.EndOfStream == false)
            {
                if (serviceReader.Peek() != 37) //ignores the line if it begins with a percent sign
                {
                    serviceList.Add(serviceReader.ReadLine());
                }
                else
                {
                    serviceReader.ReadLine();
                }
            }
            string[] serviceStrings = serviceList.ToArray();
            InitArray(serviceStrings);

            Console.ReadLine();
        }

        //initialises processes with locations based on strings in an array
        static void InitArray(string[] serviceList)
        {


            if (serviceList.Length > 0)
            {


                for (int num = 0; num < serviceList.Length; num++)
                {
                    Console.WriteLine("starting process from: {0}", serviceList[num]);
                    try
                    {
                        Process.Start(serviceList[num]);
                    }
                    catch (System.ComponentModel.Win32Exception e)
                    {
                        Console.WriteLine("ERROR: error opening process; {0}", e.Message);
                    }

                }
            }
            else
            {
                Console.WriteLine("ERROR: Cannot find anything to open. make sure ResourceList.txt is configured properly and is" +
                    " in the same directory");
            }
        }
    }
}
